﻿using System;

namespace DataSQL
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProductDescription { get; set; }
        public DateTime PublicationDate { get; set; }
        public string City { get; set; }
        public int Price { get; set; }
        public string ContactNumber { get; set; }
        public string SellerName { get; set; }    
        public int ProductSubcategoryId { get; set; }

        public Product() { }
       
        public Product(int id, string name, string productDescription, DateTime publicationDate, string city, int price, string contactNumber, string sellerName, int productSubcategoryId)
        {
            Id = id;
            Name = name;
            ProductDescription = productDescription;
            PublicationDate = publicationDate;
            City = city;
            Price = price;
            ContactNumber = contactNumber;
            SellerName = sellerName;
            ProductSubcategoryId = productSubcategoryId;
        }

        public virtual ProductSubcategory ProductSubcategories { get; set; }

        public override string ToString()
        {
            return $"{Name}\t{ProductDescription}\t{PublicationDate}  {City} Цена: {Price} руб. тел.{ContactNumber}  {SellerName}";
        }
    }
}
