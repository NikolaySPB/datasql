﻿using System.Collections.Generic;

namespace DataSQL
{
    public class ProductCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ProductCategory() { }        

        public ProductCategory(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public virtual ICollection<ProductSubcategory> ProductSubcategories { get; set; }
    }
}
