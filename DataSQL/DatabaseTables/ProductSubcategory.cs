﻿using System.Collections.Generic;

namespace DataSQL
{
    public class ProductSubcategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProductCategoryId { get; set; }

        public ProductSubcategory() { }

        public ProductSubcategory(int id, string name, int productCategoryId)
        {
            Id = id;
            Name = name;
            ProductCategoryId = productCategoryId;
        }

        public virtual ProductCategory ProductCategory { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
