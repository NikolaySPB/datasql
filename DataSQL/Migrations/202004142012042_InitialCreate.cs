﻿namespace DataSQL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductSubcategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProductCategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductCategories", t => t.ProductCategoryId, cascadeDelete: true)
                .Index(t => t.ProductCategoryId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProductDescription = c.String(),
                        PublicationDate = c.DateTime(nullable: false),
                        City = c.String(),
                        Price = c.Int(nullable: false),
                        ContactNumber = c.String(),
                        SellerName = c.String(),
                        ProductSubcategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductSubcategories", t => t.ProductSubcategoryId, cascadeDelete: true)
                .Index(t => t.ProductSubcategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ProductSubcategoryId", "dbo.ProductSubcategories");
            DropForeignKey("dbo.ProductSubcategories", "ProductCategoryId", "dbo.ProductCategories");
            DropIndex("dbo.Products", new[] { "ProductSubcategoryId" });
            DropIndex("dbo.ProductSubcategories", new[] { "ProductCategoryId" });
            DropTable("dbo.Products");
            DropTable("dbo.ProductSubcategories");
            DropTable("dbo.ProductCategories");
        }
    }
}
