﻿using System;
using System.Linq;

namespace DataSQL
{
    public class JobDatabase
    {
        private void ColorHeading(string text, Newline newline)
        {
            if (newline == Newline.Yes)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine(text);
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.Write(text);
                Console.ResetColor();
            }
        }
        public void CheckContactPhoneNumber(MyDataSqlContext context, string phoneNumber)
        {
            ColorHeading($"Итог проверки наличия телефонного номера {phoneNumber} в базе: ", Newline.No);

            Console.WriteLine(context.Products.Any(item => item.ContactNumber == phoneNumber));
        }
        public void SumOfAllAdPrices(MyDataSqlContext context)
        {
            ColorHeading($"Сумма все цен:", Newline.No);

            Console.WriteLine($"{context.Products.Select(x => x.Price).Sum()} руб.");
        }
        public void FilterAdsByPrice(MyDataSqlContext context, int minPrice, int maxPrice)
        {
            ColorHeading($"Объявлений с объявленной ценой больше {minPrice} руб.и меньше {maxPrice} руб.", Newline.Yes);

            var range = from item in context.Products
                        where item.Price > minPrice && item.Price < maxPrice
                        select item;

            foreach (var item in range)
            {
                Console.WriteLine(item);
            }
        }
        public void CallAllAdCategories(MyDataSqlContext context)
        {
            ColorHeading("Все категории объявлений", Newline.Yes);

            foreach (var item in context.ProductCategories)
            {
                Console.WriteLine(item.Name.ToString());
            }
        }
        public void CallAllSubcategoriesOfAds(MyDataSqlContext context)
        {
            ColorHeading("Все под категории объявлений", Newline.Yes);

            foreach (var item in context.ProductSubcategories)
            {
                Console.WriteLine(item.Name.ToString());
            }
        }
        public void AllListingsWithTheSpecifiedContactName(MyDataSqlContext context, string name)
        {
            ColorHeading($"Все объявления с указанным контактным именем {name}", Newline.Yes);

            var rangeName = from item in context.Products
                            where item.SellerName == name
                            select item;

            foreach (var item in rangeName)
            {
                Console.WriteLine(item);
            }
        }
        public void SortAdsByPrice(MyDataSqlContext context)
        {
            ColorHeading("Сортировка объявлений по цене", Newline.Yes);

            var products = context.Products.GroupBy(item => item.Price);

            foreach (var grup in products)
            {
                foreach (var item in grup)
                {
                    Console.WriteLine(item);
                }
            }
        }
    }
}
