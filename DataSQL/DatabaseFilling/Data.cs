﻿using System;
using System.Collections.Generic;

namespace DataSQL
{
    public class Data
    {
        public void RecordProductCategory(List<ProductCategory> productCategories)
        {
            productCategories.Add(new ProductCategory() { Name = "Транспорт" });
            productCategories.Add(new ProductCategory() { Name = "Недвижимость" });
            productCategories.Add(new ProductCategory() { Name = "Работа" });
            productCategories.Add(new ProductCategory() { Name = "Личные вещи" });
            productCategories.Add(new ProductCategory() { Name = "Для дома и дачи" });
            productCategories.Add(new ProductCategory() { Name = "Для бизнеса" });
            productCategories.Add(new ProductCategory() { Name = "Бытовая электроника" });
            productCategories.Add(new ProductCategory() { Name = "Хобби и отдых" });
            productCategories.Add(new ProductCategory() { Name = "Животные" });
            productCategories.Add(new ProductCategory() { Name = "Услуги" });
        }

        public void RecordProductSubcategory(List<ProductSubcategory> productSubcategories, List<ProductCategory> collectionOfCategories)
        {
            productSubcategories.Add(new ProductSubcategory() { Name = "Автомобили", ProductCategoryId = collectionOfCategories[0].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Мотоциклы и мототехника", ProductCategoryId = collectionOfCategories[0].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Грузовики и спецтехника", ProductCategoryId = collectionOfCategories[0].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Водный транспорт", ProductCategoryId = collectionOfCategories[0].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Запчасти и аксессуары", ProductCategoryId = collectionOfCategories[0].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Квартиры в новостройках", ProductCategoryId = collectionOfCategories[1].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Квартиры в аренду", ProductCategoryId = collectionOfCategories[1].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Квартиры посуточно", ProductCategoryId = collectionOfCategories[1].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Дома, дачи, коттеджи", ProductCategoryId = collectionOfCategories[1].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Комнаты", ProductCategoryId = collectionOfCategories[1].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Коммерческая недвижимость", ProductCategoryId = collectionOfCategories[1].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Вакансии", ProductCategoryId = collectionOfCategories[2].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Отрасли", ProductCategoryId = collectionOfCategories[2].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Резюме", ProductCategoryId = collectionOfCategories[2].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Одежда, обувь, аксессуары", ProductCategoryId = collectionOfCategories[3].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Детская одежда и обувь", ProductCategoryId = collectionOfCategories[3].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Товары для детей и игрушки", ProductCategoryId = collectionOfCategories[3].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Часы и украшения", ProductCategoryId = collectionOfCategories[3].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Красота и здоровье", ProductCategoryId = collectionOfCategories[3].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Бытовая техника", ProductCategoryId = collectionOfCategories[4].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Мебель и интерьер", ProductCategoryId = collectionOfCategories[4].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Посуда и товары для кухни", ProductCategoryId = collectionOfCategories[4].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Продукты питания", ProductCategoryId = collectionOfCategories[4].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Ремонт и строительство", ProductCategoryId = collectionOfCategories[4].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Растения", ProductCategoryId = collectionOfCategories[4].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Готовый бизнес", ProductCategoryId = collectionOfCategories[5].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Оборудование для бизнеса", ProductCategoryId = collectionOfCategories[5].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Аудио и видео", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Игры, приставки и программы", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Настольные компьютеры", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Ноутбуки", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Оргтехника и расходники", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Планшеты и электронные книги", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Телефоны", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Товары для компьютера", ProductCategoryId = collectionOfCategories[6].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Фототехника", ProductCategoryId = collectionOfCategories[6].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Билеты и путешествия", ProductCategoryId = collectionOfCategories[7].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Велосипеды", ProductCategoryId = collectionOfCategories[7].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Книги и журналы", ProductCategoryId = collectionOfCategories[7].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Коллекционирование", ProductCategoryId = collectionOfCategories[7].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Музыкальные инструменты", ProductCategoryId = collectionOfCategories[7].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Охота и рыбалка", ProductCategoryId = collectionOfCategories[7].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Спорт и отдых", ProductCategoryId = collectionOfCategories[7].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "Собаки", ProductCategoryId = collectionOfCategories[8].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Кошки", ProductCategoryId = collectionOfCategories[8].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Птицы", ProductCategoryId = collectionOfCategories[8].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Аквариум", ProductCategoryId = collectionOfCategories[8].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Другие животные", ProductCategoryId = collectionOfCategories[8].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Товары для животных", ProductCategoryId = collectionOfCategories[8].Id });

            productSubcategories.Add(new ProductSubcategory() { Name = "IT, интернет, телеком", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Бытовые услуги", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Деловые услуги", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Искусство", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Красота, здоровье", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Доставка, курьеры", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Мастер на час", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Няни, сиделки", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Оборудование, производство", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Обучение, курсы", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Охрана, безопасность", ProductCategoryId = collectionOfCategories[9].Id });
            productSubcategories.Add(new ProductSubcategory() { Name = "Доставка еды и продуктов", ProductCategoryId = collectionOfCategories[9].Id });
        }

        public void RecordProduct(List<Product> products, List<ProductSubcategory> productSubcategories)
        {
            Random rnd = new Random();

            string[] rndString = new string[10] { "TDRHJ", "JSDB", "RSBS", "PSBS", "AEGAF", "OSBS", "BSBGN", "ISBS", "XKLYU", "AESGS" };
            string[] rndName = new string[10] { "Вячеслав", "Георгий", "Емельян", "Максим", "Моисей", "Вера", "Екатерина", "Светлана", "Наталья", "Людмила" };

            for (int i = 0; i < 61; i++)
            {
                int x = rnd.Next(5, 16);
                for (int j = 0; j < x; j++)
                {
                    products.Add(new Product()
                    {
                        Name = $"Название: " + rndString[rnd.Next(0, rndString.Length - 1)],
                        ProductDescription = $"Описание товара:" + rndString[rnd.Next(0, rndString.Length - 1)],
                        PublicationDate = DateTime.Now.AddDays(-rnd.Next(0, 180)),
                        City = $"Город под № региона {rnd.Next(10, 99)}",
                        Price = rnd.Next(10_000, 1_000_000),
                        ContactNumber = $"89{rnd.Next(10, 100)}{rnd.Next(100, 1000)}{rnd.Next(1000, 10000)}",
                        SellerName = rndName[rnd.Next(0, rndName.Length - 1)],
                        ProductSubcategoryId = productSubcategories[i].Id
                    });
                }
            }
        }
    }
}
