﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DataSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new MyDataSqlContext())
            {
                #region Заполнение БД категориями, под категориями и товарами(услугами)

                var collectionOfCategories = new List<ProductCategory>();
                var collectionOfSubcategories = new List<ProductSubcategory>();
                var collectionProduct = new List<Product>();

                var data = new Data();

                data.RecordProductCategory(collectionOfCategories);
                data.RecordProductSubcategory(collectionOfSubcategories, collectionOfCategories);
                data.RecordProduct(collectionProduct, collectionOfSubcategories);
                
                #endregion

                #region Стандартные запросы
                JobDatabase jobSQL = new JobDatabase();

                // Запрос на проверку телефонного номера. Если искомый телефонный номер есть хотя бы в одном объявлении, то получит True, в противном случае False
                jobSQL.CheckContactPhoneNumber(context, "89520546585");
                jobSQL.CheckContactPhoneNumber(context, "89128264306");

                // Сумма всеx цен объявлений
                jobSQL.SumOfAllAdPrices(context);

                // Объявлений с объявленной ценой больше 50 тыс.руб.и меньше 70 тыс.руб. 
                jobSQL.FilterAdsByPrice(context, 50000, 70000);

                // Запрос на вывод всех категорий объявлений
                jobSQL.CallAllAdCategories(context);

                // Запрос на вывод всех под категорий объявлений
                jobSQL.CallAllSubcategoriesOfAds(context);

                // Все объявления которые подавали продавцы с именем Вячеслав                
                jobSQL.AllListingsWithTheSpecifiedContactName(context, "Вячеслав");

                // Сортировка объявлений по цене 
                jobSQL.SortAdsByPrice(context);

                #endregion                
            }

            #region Запросы через паттерн репозиторий EF

            Console.WriteLine("\nProductCategory\n");

            SQLProductCategoryRepository productCategory = new SQLProductCategoryRepository();

            foreach (var item in productCategory.GetList())
            {
                Console.WriteLine($"{item.Id} {item.Name}");
            }

            Console.WriteLine("\nProductSubcategory\n");

            SQLProductSubcategoryRepository productSubcategory = new SQLProductSubcategoryRepository();

            foreach (var item in productSubcategory.GetList())
            {
                if (item.Id <= 10)
                {
                    Console.WriteLine($"{item.Id} {item.Name}");
                }
            }

            Console.WriteLine("\nProduct\n");

            SQLProductRepository product = new SQLProductRepository();

            foreach (var item in product.GetList())
            {
                if (item.Id <= 10)
                {
                    Console.WriteLine($"{item.Id} {item.Name} {item.ProductDescription} {item.PublicationDate} " +
                        $"{item.City} {item.Price} {item.ContactNumber} {item.SellerName}");
                }
            }

            #endregion

            #region Запросы через паттерн репозиторий ADO.Net

            ProductCategory productCategory1 = new ProductCategory();
            productCategory1.Name = "Тест категория";

            ProductSubcategory productSubcategory1 = new ProductSubcategory();
            productSubcategory1.Name = "Тест под категория";

            Product product1 = new Product();
            product1.Name = "Тест товар";
            product1.ProductDescription = "Тест описание, характеристика";
            product1.PublicationDate = DateTime.Now;
            product1.City = "Тест город";
            product1.Price = 111111;
            product1.ContactNumber = "89056589562";
            product1.SellerName = "Тест имя";


            AdoNetRepoPC adoNetRepoPC = new AdoNetRepoPC();

            adoNetRepoPC.GetList();
            adoNetRepoPC.Get(3);
            adoNetRepoPC.Create(productCategory1);
            adoNetRepoPC.Update(productCategory1);
            adoNetRepoPC.Delete(11);

            AdoNetRepoPSC adoNetRepoPSC = new AdoNetRepoPSC();

            adoNetRepoPSC.GetList();
            adoNetRepoPSC.Get(3);
            adoNetRepoPSC.Create(productSubcategory1);
            adoNetRepoPSC.Update(productSubcategory1);
            adoNetRepoPSC.Delete(11);


            AdoNetRepoP adoNetRepoP = new AdoNetRepoP();

            adoNetRepoP.GetList();
            adoNetRepoP.Get(3);
            adoNetRepoP.Create(product1);
            adoNetRepoP.Update(product1);
            adoNetRepoP.Delete(11);

            #endregion

            Console.ReadLine();
        }
    }
}
