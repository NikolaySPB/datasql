﻿using System.Data.Entity;

namespace DataSQL
{
    public class MyDataSqlContext : DbContext
    {
        public MyDataSqlContext() : base("DataSqlConnectionString")
        { }

        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductSubcategory> ProductSubcategories { get; set; }
        public DbSet<Product> Products { get; set; }        
    }
}
