﻿using System.Web.Mvc;

namespace DataSQL
{
    public class ProductSubcategoryController : Controller
    {
        IRepository<ProductSubcategory> db;

        public ProductSubcategoryController()
        {
            db = new SQLProductSubcategoryRepository();
        }

        public ActionResult Index()
        {
            return View(db.GetList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProductSubcategory productSubcategory)
        {
            if (ModelState.IsValid)
            {
                db.Create(productSubcategory);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(productSubcategory);
        }

        public ActionResult Edit(int id)
        {
            ProductSubcategory productSubcategory = db.Get(id);
            return View(productSubcategory);
        }

        [HttpPost]
        public ActionResult Edit(ProductSubcategory productSubcategory)
        {
            if (ModelState.IsValid)
            {
                db.Update(productSubcategory);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(productSubcategory);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ProductSubcategory p = db.Get(id);
            return View(p);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}

