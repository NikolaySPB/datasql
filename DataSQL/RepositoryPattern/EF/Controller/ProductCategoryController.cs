﻿using System.Web.Mvc;

namespace DataSQL
{
    public class ProductCategoryController : Controller
    {
        IRepository<ProductCategory> db;

        public ProductCategoryController()
        {
            db = new SQLProductCategoryRepository();
        }

        public ActionResult Index()
        {
            return View(db.GetList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProductCategory productCategory)
        {
            if (ModelState.IsValid)
            {
                db.Create(productCategory);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(productCategory);
        }

        public ActionResult Edit(int id)
        {
            ProductCategory productCategory = db.Get(id);
            return View(productCategory);
        }

        [HttpPost]
        public ActionResult Edit(ProductCategory productCategory)
        {
            if (ModelState.IsValid)
            {
                db.Update(productCategory);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(productCategory);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ProductCategory p = db.Get(id);
            return View(p);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
