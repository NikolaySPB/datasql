﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DataSQL
{
    public class SQLProductSubcategoryRepository : IRepository<ProductSubcategory>
    {
        private MyDataSqlContext db;

        public SQLProductSubcategoryRepository()
        {
            db = new MyDataSqlContext();
        }

        public IEnumerable<ProductSubcategory> GetList()
        {
            return db.ProductSubcategories;
        }

        public ProductSubcategory Get(int id)
        {
            return db.ProductSubcategories.Find(id);
        }

        public void Create(ProductSubcategory productSubcategory)
        {
            db.ProductSubcategories.Add(productSubcategory);
        }

        public void Update(ProductSubcategory productSubcategory)
        {
            db.Entry(productSubcategory).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            ProductSubcategory productSubcategory = db.ProductSubcategories.Find(id);
            if (productSubcategory != null)
                db.ProductSubcategories.Remove(productSubcategory);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
