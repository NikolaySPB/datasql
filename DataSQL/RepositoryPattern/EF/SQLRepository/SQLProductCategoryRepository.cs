﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DataSQL
{
    public class SQLProductCategoryRepository : IRepository<ProductCategory>
    {
        private MyDataSqlContext db;

        public SQLProductCategoryRepository()
        {
            db = new MyDataSqlContext();
        }

        public IEnumerable<ProductCategory> GetList()
        {
            return db.ProductCategories;
        }

        public ProductCategory Get(int id)
        {
            return db.ProductCategories.Find(id);
        }

        public void Create(ProductCategory productCategory)
        {
            db.ProductCategories.Add(productCategory);
        }

        public void Update(ProductCategory productCategory)
        {
            db.Entry(productCategory).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            ProductCategory productCategory = db.ProductCategories.Find(id);
            if (productCategory != null)
                db.ProductCategories.Remove(productCategory);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
