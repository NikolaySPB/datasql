﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSQL
{
    public class AdoNetRepoPC : IRepository<ProductCategory>
    {
        private const string connectionString = @"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog=Avito; Integrated Security=True; Pooling = true";
        
        public IEnumerable<ProductCategory> GetList()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand();

                command.CommandText = $"SELECT * FROM ProductCategories";

                command.Connection = connection;

                SqlDataReader reader = command.ExecuteReader();

                List<ProductCategory> collection = new List<ProductCategory>();

                if (reader.HasRows) 
                {                    
                    Console.WriteLine("{0}\t{1}", reader.GetName(0), reader.GetName(1));

                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);

                        string name = reader.GetString(1);                        

                        collection.Add(new ProductCategory(id, name));

                        Console.WriteLine("{0} \t{1}", id, name);
                    }
                }

                reader.Close();
                
                return collection;
            }
        }

        public ProductCategory Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand();

                command.CommandText = $"SELECT * FROM ProductCategories WHERE Id = {id}";

                command.Connection = connection;

                SqlDataReader reader = command.ExecuteReader();

                ProductCategory productCategory = new ProductCategory();

                if (reader.HasRows)
                {
                    Console.WriteLine("{0}\t{1}", reader.GetName(0), reader.GetName(1));

                    while (reader.Read())
                    {
                        productCategory.Id = reader.GetInt32(0);

                        productCategory.Name = reader.GetString(1);                        

                        Console.WriteLine("{0} \t{1}", productCategory.Id, productCategory.Name);
                    }
                }

                reader.Close();
                
                return productCategory;
            }
        }

        public void Create(ProductCategory productCategory)
        {
            string sqlExpression = $"INSERT INTO ProductCategories (Name) VALUES ('{productCategory.Name}')";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Добавлено объектов: {0}", number);
            }
        }

        public void Update(ProductCategory productCategory)
        {
            string sqlExpression = $"UPDATE ProductCategories SET Name='{productCategory.Name}' WHERE Id={productCategory.Id}"; 

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Обновлено объектов: {0}", number);
            }
        }

        public void Delete(int id)
        {
            string sqlExpression = $"DELETE  FROM ProductCategories WHERE Id={id}";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Удалено объектов: {0}", number);
            }
        }
        public void Save()
        {
        }

        public virtual void Dispose(bool disposing)
        {
        }
        public void Dispose()
        {
        }
    }
}
