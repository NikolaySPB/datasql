﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSQL
{
    public class AdoNetRepoPSC : IRepository<ProductSubcategory>
    {
        private const string connectionString = @"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog=Avito; Integrated Security=True; Pooling = true";

        public IEnumerable<ProductSubcategory> GetList()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand();

                command.CommandText = $"SELECT * FROM ProductSubcategories";

                command.Connection = connection;

                SqlDataReader reader = command.ExecuteReader();

                List<ProductSubcategory> collection = new List<ProductSubcategory>();

                if (reader.HasRows)
                {
                    Console.WriteLine("{0}\t{1}\t{2}", reader.GetName(0), reader.GetName(1), reader.GetName(2));

                    while (reader.Read()) 
                    {
                        int id = reader.GetInt32(0);

                        string name = reader.GetString(1);

                        int productCategoryId = reader.GetInt32(0);

                        collection.Add(new ProductSubcategory(id, name, productCategoryId));

                        Console.WriteLine("{0}\t{1}\t{2}", id, name, productCategoryId);
                    }
                }

                reader.Close();

                return collection;
            }
        }

        public ProductSubcategory Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand();

                command.CommandText = $"SELECT * FROM ProductSubcategories WHERE Id = {id}";

                command.Connection = connection;

                SqlDataReader reader = command.ExecuteReader();

                ProductSubcategory productSubcategory = new ProductSubcategory();

                if (reader.HasRows)
                {
                    Console.WriteLine("{0}\t{1}", reader.GetName(0), reader.GetName(1));

                    while (reader.Read()) // построчно считываем данные
                    {
                        productSubcategory.Id = reader.GetInt32(0);

                        productSubcategory.Name = reader.GetString(1);

                        productSubcategory.ProductCategoryId = reader.GetInt32(2);

                        Console.WriteLine("{0}\t{1}\t{2}", productSubcategory.Id, productSubcategory.Name, productSubcategory.ProductCategoryId);
                    }
                }

                reader.Close();

                return productSubcategory;
            }
        }

        public void Create(ProductSubcategory productSubcategory)
        {
            string sqlExpression = $"INSERT INTO ProductSubcategories (Name) VALUES ('{productSubcategory.Name}')";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Добавлено объектов: {0}", number);
            }
        }

        public void Update(ProductSubcategory productSubcategory)
        {
            string sqlExpression = $"UPDATE ProductSubcategories SET Name='{productSubcategory.Name}' WHERE Id={productSubcategory.Id}";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Обновлено объектов: {0}", number);
            }
        }

        public void Delete(int id)
        {
            string sqlExpression = $"DELETE  FROM ProductSubcategories WHERE Id={id}";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Удалено объектов: {0}", number);
            }
        }
        public void Save()
        {
        }

        public virtual void Dispose(bool disposing)
        {
        }
        public void Dispose()
        {
        }
    }
}
