﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataSQL
{
    public class AdoNetRepoP : IRepository<Product>
    {
        private const string connectionString = @"Data Source=(localdb)\MSSQLLocalDB; Initial Catalog=Avito; Integrated Security=True; Pooling = true";

        public IEnumerable<Product> GetList()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand();

                command.CommandText = $"SELECT * FROM Products";

                command.Connection = connection;

                SqlDataReader reader = command.ExecuteReader();

                List<Product> collection = new List<Product>();

                if (reader.HasRows)
                {
                    Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}",
                        reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4),
                        reader.GetName(5), reader.GetName(6), reader.GetName(7), reader.GetName(8));

                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);

                        string name = reader.GetString(1);

                        string productDescription = reader.GetString(2);

                        DateTime publicationDate = reader.GetDateTime(3);

                        string city = reader.GetString(4);

                        int price = reader.GetInt32(5);

                        string contactNumber = reader.GetString(6);

                        string sellerName = reader.GetString(7);

                        int productSubcategoryId = reader.GetInt32(8);

                        collection.Add(new Product(id, name, productDescription, publicationDate, city, price, contactNumber, sellerName, productSubcategoryId));

                        Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}",
                            id, name, productDescription, publicationDate, city, price, contactNumber, sellerName, productSubcategoryId);
                    }
                }

                reader.Close();

                return collection;
            }
        }

        public Product Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand();

                command.CommandText = $"SELECT * FROM Products WHERE Id = {id}";

                command.Connection = connection;

                SqlDataReader reader = command.ExecuteReader();

                Product product = new Product();

                if (reader.HasRows)
                {
                    Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}",
                        reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4),
                        reader.GetName(5), reader.GetName(6), reader.GetName(7), reader.GetName(8));

                    while (reader.Read())
                    {
                        product.Id = reader.GetInt32(0);

                        product.Name = reader.GetString(1);

                        product.ProductDescription = reader.GetString(2);

                        product.PublicationDate = reader.GetDateTime(3);

                        product.City = reader.GetString(4);

                        product.Price = reader.GetInt32(5);

                        product.ContactNumber = reader.GetString(6);

                        product.SellerName = reader.GetString(7);

                        product.ProductSubcategoryId = reader.GetInt32(8);

                        Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}",
                            product.Id, product.Name, product.ProductDescription, product.PublicationDate,
                            product.City, product.Price, product.ContactNumber, product.SellerName, product.ProductSubcategoryId);
                    }
                }

                reader.Close();

                return product;
            }
        }

        public void Create(Product product)
        {
            string sqlExpression = $"INSERT INTO Products (Name, ProductDescription, PublicationDate, City, Price, ContactNumber, SellerName) " +
                $"VALUES ('{product.Name}', '{product.ProductDescription}', {product.PublicationDate}, '{product.City}', {product.Price}, '{product.ContactNumber}', '{product.SellerName}')";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Добавлено объектов: {0}", number);
            }
        }

        public void Update(Product product)
        {
            string sqlExpression = $"UPDATE Products SET " +
                $"Name='{product.Name}', " +
                $"ProductDescription='{product.ProductDescription}', " +
                $"PublicationDate={product.PublicationDate}, " +
                $"City='{product.City}', " +
                $"Price={product.Price}, " +
                $"ContactNumber='{product.ContactNumber}', " +
                $"SellerName='{product.SellerName}' " +
                $"WHERE Id={product.Id}";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                int number = command.ExecuteNonQuery();
                Console.WriteLine("Обновлено объектов: {0}", number);
            }
        }

        public void Delete(int id)
        {
            string sqlExpression = $"DELETE  FROM Products WHERE Id={id}";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = command.ExecuteNonQuery();

                Console.WriteLine("Удалено объектов: {0}", number);
            }
        }

        public void Save()
        {
        }

        public virtual void Dispose(bool disposing)
        {
        }
        public void Dispose()
        {
        }
    }
}
